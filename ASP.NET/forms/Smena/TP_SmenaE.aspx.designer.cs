﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace IIS.AutoService {
    
    
    public partial class TP_СменаE {
        
        /// <summary>
        /// ctrlДатаИВремяНачалаLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДатаИВремяНачалаLabel;
        
        /// <summary>
        /// ctrlДатаИВремяНачала элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePicker ctrlДатаИВремяНачала;
        
        /// <summary>
        /// ctrlДатаИВремяНачалаRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlДатаИВремяНачалаRequiredFieldValidator;
        
        /// <summary>
        /// ctrlДатаИВремяНачалаDatePickerValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePickerValidator ctrlДатаИВремяНачалаDatePickerValidator;
        
        /// <summary>
        /// ctrlДатаИВремяКонцаLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДатаИВремяКонцаLabel;
        
        /// <summary>
        /// ctrlДатаИВремяКонца элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePicker ctrlДатаИВремяКонца;
        
        /// <summary>
        /// ctrlДатаИВремяКонцаDatePickerValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePickerValidator ctrlДатаИВремяКонцаDatePickerValidator;
        
        /// <summary>
        /// ctrlВыручкаLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlВыручкаLabel;
        
        /// <summary>
        /// ctrlВыручка элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.AlphaNumericTextBox ctrlВыручка;
        
        /// <summary>
        /// ctrlМенеджерLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlМенеджерLabel;
        
        /// <summary>
        /// ctrlМенеджер элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.MasterEditorAjaxLookUp ctrlМенеджер;
        
        /// <summary>
        /// ctrlМенеджерRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlМенеджерRequiredFieldValidator;
        
        /// <summary>
        /// ctrlМенеджер_НомерLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlМенеджер_НомерLabel;
        
        /// <summary>
        /// ctrlМенеджер_Номер элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.AlphaNumericTextBox ctrlМенеджер_Номер;
        
        /// <summary>
        /// ctrlМенеджер_НомерRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlМенеджер_НомерRequiredFieldValidator;
        
        /// <summary>
        /// ctrlМенеджер_ФамилияLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlМенеджер_ФамилияLabel;
        
        /// <summary>
        /// ctrlМенеджер_Фамилия элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ctrlМенеджер_Фамилия;
        
        /// <summary>
        /// ctrlМенеджер_ФамилияRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlМенеджер_ФамилияRequiredFieldValidator;
    }
}
