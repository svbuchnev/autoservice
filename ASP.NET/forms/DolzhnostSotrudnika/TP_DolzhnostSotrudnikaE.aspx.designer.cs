﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace IIS.AutoService {
    
    
    public partial class TP_ДолжностьСотрудникаE {
        
        /// <summary>
        /// ctrlСотрудникLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlСотрудникLabel;
        
        /// <summary>
        /// ctrlСотрудник элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.MasterEditorAjaxLookUp ctrlСотрудник;
        
        /// <summary>
        /// ctrlСотрудникRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlСотрудникRequiredFieldValidator;
        
        /// <summary>
        /// ctrlСотрудник_НомерLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlСотрудник_НомерLabel;
        
        /// <summary>
        /// ctrlСотрудник_Номер элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.AlphaNumericTextBox ctrlСотрудник_Номер;
        
        /// <summary>
        /// ctrlСотрудник_НомерRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlСотрудник_НомерRequiredFieldValidator;
        
        /// <summary>
        /// ctrlДолжностьLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДолжностьLabel;
        
        /// <summary>
        /// ctrlДолжность элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.MasterEditorAjaxLookUp ctrlДолжность;
        
        /// <summary>
        /// ctrlДолжностьRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlДолжностьRequiredFieldValidator;
        
        /// <summary>
        /// ctrlДолжность_НазваниеLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДолжность_НазваниеLabel;
        
        /// <summary>
        /// ctrlДолжность_Название элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ctrlДолжность_Название;
        
        /// <summary>
        /// ctrlДолжность_НазваниеRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlДолжность_НазваниеRequiredFieldValidator;
        
        /// <summary>
        /// ctrlДатаВступленияLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДатаВступленияLabel;
        
        /// <summary>
        /// ctrlДатаВступления элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePicker ctrlДатаВступления;
        
        /// <summary>
        /// ctrlДатаВступленияRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlДатаВступленияRequiredFieldValidator;
        
        /// <summary>
        /// ctrlДатаВступленияDatePickerValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePickerValidator ctrlДатаВступленияDatePickerValidator;
        
        /// <summary>
        /// ctrlДатаОставленияLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДатаОставленияLabel;
        
        /// <summary>
        /// ctrlДатаОставления элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePicker ctrlДатаОставления;
        
        /// <summary>
        /// ctrlДатаОставленияDatePickerValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePickerValidator ctrlДатаОставленияDatePickerValidator;
    }
}
