﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace IIS.AutoService {
    
    
    public partial class TP_СотрудникE {
        
        /// <summary>
        /// ctrlНомерLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlНомерLabel;
        
        /// <summary>
        /// ctrlНомер элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.AlphaNumericTextBox ctrlНомер;
        
        /// <summary>
        /// ctrlНомерRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlНомерRequiredFieldValidator;
        
        /// <summary>
        /// ctrlДатаНаймаLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДатаНаймаLabel;
        
        /// <summary>
        /// ctrlДатаНайма элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePicker ctrlДатаНайма;
        
        /// <summary>
        /// ctrlДатаНаймаRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlДатаНаймаRequiredFieldValidator;
        
        /// <summary>
        /// ctrlДатаНаймаDatePickerValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePickerValidator ctrlДатаНаймаDatePickerValidator;
        
        /// <summary>
        /// ctrlДатаУвольненияLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДатаУвольненияLabel;
        
        /// <summary>
        /// ctrlДатаУвольнения элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePicker ctrlДатаУвольнения;
        
        /// <summary>
        /// ctrlДатаУвольненияDatePickerValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePickerValidator ctrlДатаУвольненияDatePickerValidator;
        
        /// <summary>
        /// ctrlФамилияLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlФамилияLabel;
        
        /// <summary>
        /// ctrlФамилия элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ctrlФамилия;
        
        /// <summary>
        /// ctrlФамилияRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlФамилияRequiredFieldValidator;
        
        /// <summary>
        /// ctrlИмяLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlИмяLabel;
        
        /// <summary>
        /// ctrlИмя элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ctrlИмя;
        
        /// <summary>
        /// ctrlИмяRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlИмяRequiredFieldValidator;
        
        /// <summary>
        /// ctrlОтчествоLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlОтчествоLabel;
        
        /// <summary>
        /// ctrlОтчество элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ctrlОтчество;
        
        /// <summary>
        /// ctrlОтчествоRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlОтчествоRequiredFieldValidator;
        
        /// <summary>
        /// ctrlАвтосервисLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlАвтосервисLabel;
        
        /// <summary>
        /// ctrlАвтосервис элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.MasterEditorAjaxLookUp ctrlАвтосервис;
        
        /// <summary>
        /// ctrlАвтосервисRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlАвтосервисRequiredFieldValidator;
        
        /// <summary>
        /// ctrlАвтосервис_НомерLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlАвтосервис_НомерLabel;
        
        /// <summary>
        /// ctrlАвтосервис_Номер элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.AlphaNumericTextBox ctrlАвтосервис_Номер;
        
        /// <summary>
        /// ctrlАвтосервис_НомерRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlАвтосервис_НомерRequiredFieldValidator;
        
        /// <summary>
        /// ctrlАктуальноLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlАктуальноLabel;
        
        /// <summary>
        /// ctrlАктуально элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox ctrlАктуально;
        
        /// <summary>
        /// ScriptManager1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.ScriptManager ScriptManager1;
        
        /// <summary>
        /// ctrlДолжностьСотрудника элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.AjaxGroupEdit ctrlДолжностьСотрудника;
    }
}
