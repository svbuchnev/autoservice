﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace IIS.AutoService {
    
    
    public partial class TP_ЗаказE {
        
        /// <summary>
        /// ctrlНомерLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlНомерLabel;
        
        /// <summary>
        /// ctrlНомер элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.AlphaNumericTextBox ctrlНомер;
        
        /// <summary>
        /// ctrlНомерRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlНомерRequiredFieldValidator;
        
        /// <summary>
        /// ctrlКлиентLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlКлиентLabel;
        
        /// <summary>
        /// ctrlКлиент элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.MasterEditorAjaxLookUp ctrlКлиент;
        
        /// <summary>
        /// ctrlКлиентRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlКлиентRequiredFieldValidator;
        
        /// <summary>
        /// ctrlКлиент_НомерПаспортаLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlКлиент_НомерПаспортаLabel;
        
        /// <summary>
        /// ctrlКлиент_НомерПаспорта элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.AlphaNumericTextBox ctrlКлиент_НомерПаспорта;
        
        /// <summary>
        /// ctrlКлиент_НомерПаспортаRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlКлиент_НомерПаспортаRequiredFieldValidator;
        
        /// <summary>
        /// ctrlКлиент_ФамилияLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlКлиент_ФамилияLabel;
        
        /// <summary>
        /// ctrlКлиент_Фамилия элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ctrlКлиент_Фамилия;
        
        /// <summary>
        /// ctrlКлиент_ФамилияRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlКлиент_ФамилияRequiredFieldValidator;
        
        /// <summary>
        /// ctrlАвтомобильLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlАвтомобильLabel;
        
        /// <summary>
        /// ctrlАвтомобиль элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.MasterEditorAjaxLookUp ctrlАвтомобиль;
        
        /// <summary>
        /// ctrlАвтомобильRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlАвтомобильRequiredFieldValidator;
        
        /// <summary>
        /// ctrlАвтомобиль_НомерLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlАвтомобиль_НомерLabel;
        
        /// <summary>
        /// ctrlАвтомобиль_Номер элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ctrlАвтомобиль_Номер;
        
        /// <summary>
        /// ctrlАвтомобиль_НомерRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlАвтомобиль_НомерRequiredFieldValidator;
        
        /// <summary>
        /// ctrlСтатусLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlСтатусLabel;
        
        /// <summary>
        /// ctrlСтатус элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ctrlСтатус;
        
        /// <summary>
        /// ctrlСтатусRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlСтатусRequiredFieldValidator;
        
        /// <summary>
        /// ctrlСменаОткрытияLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlСменаОткрытияLabel;
        
        /// <summary>
        /// ctrlСменаОткрытия элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.MasterEditorAjaxLookUp ctrlСменаОткрытия;
        
        /// <summary>
        /// ctrlСменаОткрытияRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlСменаОткрытияRequiredFieldValidator;
        
        /// <summary>
        /// ctrlСменаЗакрытияLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlСменаЗакрытияLabel;
        
        /// <summary>
        /// ctrlСменаЗакрытия элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.MasterEditorAjaxLookUp ctrlСменаЗакрытия;
        
        /// <summary>
        /// ctrlСменаЗакрытияRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlСменаЗакрытияRequiredFieldValidator;
        
        /// <summary>
        /// ctrlДатаИВремяОткрытияLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДатаИВремяОткрытияLabel;
        
        /// <summary>
        /// ctrlДатаИВремяОткрытия элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePicker ctrlДатаИВремяОткрытия;
        
        /// <summary>
        /// ctrlДатаИВремяОткрытияRequiredFieldValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ctrlДатаИВремяОткрытияRequiredFieldValidator;
        
        /// <summary>
        /// ctrlДатаИВремяОткрытияDatePickerValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePickerValidator ctrlДатаИВремяОткрытияDatePickerValidator;
        
        /// <summary>
        /// ctrlДатаИВремяВыполненияLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДатаИВремяВыполненияLabel;
        
        /// <summary>
        /// ctrlДатаИВремяВыполнения элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePicker ctrlДатаИВремяВыполнения;
        
        /// <summary>
        /// ctrlДатаИВремяВыполненияDatePickerValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePickerValidator ctrlДатаИВремяВыполненияDatePickerValidator;
        
        /// <summary>
        /// ctrlДатаИВремяОплатыLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlДатаИВремяОплатыLabel;
        
        /// <summary>
        /// ctrlДатаИВремяОплаты элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePicker ctrlДатаИВремяОплаты;
        
        /// <summary>
        /// ctrlДатаИВремяОплатыDatePickerValidator элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.DatePickerValidator ctrlДатаИВремяОплатыDatePickerValidator;
        
        /// <summary>
        /// ctrlКомментарийLabel элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ctrlКомментарийLabel;
        
        /// <summary>
        /// ctrlКомментарий элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ctrlКомментарий;
        
        /// <summary>
        /// ScriptManager1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.ScriptManager ScriptManager1;
        
        /// <summary>
        /// ctrlРаботаВЗаказе элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::ICSSoft.STORMNET.Web.AjaxControls.AjaxGroupEdit ctrlРаботаВЗаказе;
    }
}
