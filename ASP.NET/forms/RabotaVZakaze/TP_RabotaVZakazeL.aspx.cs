﻿/*flexberryautogenerated="true"*/
namespace IIS.AutoService
{
    using System;
    using ICSSoft.STORMNET.Web.Controls;

    using Resources;

    public partial class TP_РаботаВЗаказеL : BaseListForm<РаботаВЗаказе>
    {
        /// <summary>
        /// Конструктор без параметров,
        /// инициализирует свойства, соответствующие конкретной форме.
        /// </summary>
        public TP_РаботаВЗаказеL() : base(РаботаВЗаказе.Views.TP_РаботаВЗаказеL)
        {
            EditPage = TP_РаботаВЗаказеE.FormPath;
        }
                
        /// <summary>
        /// Путь до формы.
        /// </summary>
        public static string FormPath
        {
            get { return "~/forms/RabotaVZakaze/TP_RabotaVZakazeL.aspx"; }
        }

        /// <summary>
        /// Вызывается самым первым в Page_Load.
        /// </summary>
        protected override void Preload()
        {
        }

        /// <summary>
        /// Вызывается самым последним в Page_Load.
        /// </summary>
        protected override void Postload()
        {
        }
    }
}
