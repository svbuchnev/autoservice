﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IIS.AutoService
{
    using System;
    using System.Xml;
    using ICSSoft.STORMNET;
    
    
    // *** Start programmer edit section *** (Using statements)

    // *** End programmer edit section *** (Using statements)


    /// <summary>
    /// Работа в заказе.
    /// </summary>
    // *** Start programmer edit section *** (РаботаВЗаказе CustomAttributes)

    // *** End programmer edit section *** (РаботаВЗаказе CustomAttributes)
    [AutoAltered()]
    [Caption("Работа в заказе")]
    [AccessType(ICSSoft.STORMNET.AccessType.none)]
    [View("TP_РаботаВЗаказеE", new string[] {
            "Заказ as \'Заказ\'",
            "Заказ.Номер as \'Номер заказа\'",
            "ВидРаботы as \'Вид работы\'",
            "ВидРаботы.Название as \'Название работы\'",
            "Статус as \'Статус работы\'",
            "ДатаИВремяВыполнения as \'Дата и время выполнения\'"})]
    [MasterViewDefineAttribute("TP_РаботаВЗаказеE", "Заказ", ICSSoft.STORMNET.LookupTypeEnum.Standard, "", "Номер")]
    [MasterViewDefineAttribute("TP_РаботаВЗаказеE", "ВидРаботы", ICSSoft.STORMNET.LookupTypeEnum.Standard, "", "Название")]
    [View("TP_РаботаВЗаказеL", new string[] {
            "Заказ.Номер as \'Номер заказа\'",
            "ВидРаботы.Название as \'Название работы\'",
            "Статус as \'Статус работы\'",
            "ДатаИВремяВыполнения as \'Дата и время выполнения работы\'"})]
    public class РаботаВЗаказе : ICSSoft.STORMNET.DataObject
    {
        
        private IIS.AutoService.tСтатусРаботы fСтатус;
        
        private System.DateTime? fДатаИВремяВыполнения;
        
        private IIS.AutoService.ВидРаботы fВидРаботы;
        
        private IIS.AutoService.Заказ fЗаказ;
        
        // *** Start programmer edit section *** (РаботаВЗаказе CustomMembers)

        // *** End programmer edit section *** (РаботаВЗаказе CustomMembers)

        
        /// <summary>
        /// Статус.
        /// </summary>
        // *** Start programmer edit section *** (РаботаВЗаказе.Статус CustomAttributes)

        // *** End programmer edit section *** (РаботаВЗаказе.Статус CustomAttributes)
        [NotNull()]
        public virtual IIS.AutoService.tСтатусРаботы Статус
        {
            get
            {
                // *** Start programmer edit section *** (РаботаВЗаказе.Статус Get start)

                // *** End programmer edit section *** (РаботаВЗаказе.Статус Get start)
                IIS.AutoService.tСтатусРаботы result = this.fСтатус;
                // *** Start programmer edit section *** (РаботаВЗаказе.Статус Get end)

                // *** End programmer edit section *** (РаботаВЗаказе.Статус Get end)
                return result;
            }
            set
            {
                // *** Start programmer edit section *** (РаботаВЗаказе.Статус Set start)

                // *** End programmer edit section *** (РаботаВЗаказе.Статус Set start)
                this.fСтатус = value;
                // *** Start programmer edit section *** (РаботаВЗаказе.Статус Set end)

                // *** End programmer edit section *** (РаботаВЗаказе.Статус Set end)
            }
        }
        
        /// <summary>
        /// ДатаИВремяВыполнения.
        /// </summary>
        // *** Start programmer edit section *** (РаботаВЗаказе.ДатаИВремяВыполнения CustomAttributes)

        // *** End programmer edit section *** (РаботаВЗаказе.ДатаИВремяВыполнения CustomAttributes)
        public virtual System.DateTime? ДатаИВремяВыполнения
        {
            get
            {
                // *** Start programmer edit section *** (РаботаВЗаказе.ДатаИВремяВыполнения Get start)

                // *** End programmer edit section *** (РаботаВЗаказе.ДатаИВремяВыполнения Get start)
                System.DateTime? result = this.fДатаИВремяВыполнения;
                // *** Start programmer edit section *** (РаботаВЗаказе.ДатаИВремяВыполнения Get end)

                // *** End programmer edit section *** (РаботаВЗаказе.ДатаИВремяВыполнения Get end)
                return result;
            }
            set
            {
                // *** Start programmer edit section *** (РаботаВЗаказе.ДатаИВремяВыполнения Set start)

                // *** End programmer edit section *** (РаботаВЗаказе.ДатаИВремяВыполнения Set start)
                this.fДатаИВремяВыполнения = value;
                // *** Start programmer edit section *** (РаботаВЗаказе.ДатаИВремяВыполнения Set end)

                // *** End programmer edit section *** (РаботаВЗаказе.ДатаИВремяВыполнения Set end)
            }
        }
        
        /// <summary>
        /// Работа в заказе.
        /// </summary>
        // *** Start programmer edit section *** (РаботаВЗаказе.ВидРаботы CustomAttributes)

        // *** End programmer edit section *** (РаботаВЗаказе.ВидРаботы CustomAttributes)
        [PropertyStorage(new string[] {
                "ВидРаботы"})]
        [NotNull()]
        public virtual IIS.AutoService.ВидРаботы ВидРаботы
        {
            get
            {
                // *** Start programmer edit section *** (РаботаВЗаказе.ВидРаботы Get start)

                // *** End programmer edit section *** (РаботаВЗаказе.ВидРаботы Get start)
                IIS.AutoService.ВидРаботы result = this.fВидРаботы;
                // *** Start programmer edit section *** (РаботаВЗаказе.ВидРаботы Get end)

                // *** End programmer edit section *** (РаботаВЗаказе.ВидРаботы Get end)
                return result;
            }
            set
            {
                // *** Start programmer edit section *** (РаботаВЗаказе.ВидРаботы Set start)

                // *** End programmer edit section *** (РаботаВЗаказе.ВидРаботы Set start)
                this.fВидРаботы = value;
                // *** Start programmer edit section *** (РаботаВЗаказе.ВидРаботы Set end)

                // *** End programmer edit section *** (РаботаВЗаказе.ВидРаботы Set end)
            }
        }
        
        /// <summary>
        /// мастеровая ссылка на шапку IIS.AutoService.Заказ.
        /// </summary>
        // *** Start programmer edit section *** (РаботаВЗаказе.Заказ CustomAttributes)

        // *** End programmer edit section *** (РаботаВЗаказе.Заказ CustomAttributes)
        [Agregator()]
        [NotNull()]
        [PropertyStorage(new string[] {
                "Заказ"})]
        public virtual IIS.AutoService.Заказ Заказ
        {
            get
            {
                // *** Start programmer edit section *** (РаботаВЗаказе.Заказ Get start)

                // *** End programmer edit section *** (РаботаВЗаказе.Заказ Get start)
                IIS.AutoService.Заказ result = this.fЗаказ;
                // *** Start programmer edit section *** (РаботаВЗаказе.Заказ Get end)

                // *** End programmer edit section *** (РаботаВЗаказе.Заказ Get end)
                return result;
            }
            set
            {
                // *** Start programmer edit section *** (РаботаВЗаказе.Заказ Set start)

                // *** End programmer edit section *** (РаботаВЗаказе.Заказ Set start)
                this.fЗаказ = value;
                // *** Start programmer edit section *** (РаботаВЗаказе.Заказ Set end)

                // *** End programmer edit section *** (РаботаВЗаказе.Заказ Set end)
            }
        }
        
        /// <summary>
        /// Class views container.
        /// </summary>
        public class Views
        {
            
            /// <summary>
            /// "TP_РаботаВЗаказеE" view.
            /// </summary>
            public static ICSSoft.STORMNET.View TP_РаботаВЗаказеE
            {
                get
                {
                    return ICSSoft.STORMNET.Information.GetView("TP_РаботаВЗаказеE", typeof(IIS.AutoService.РаботаВЗаказе));
                }
            }
            
            /// <summary>
            /// "TP_РаботаВЗаказеL" view.
            /// </summary>
            public static ICSSoft.STORMNET.View TP_РаботаВЗаказеL
            {
                get
                {
                    return ICSSoft.STORMNET.Information.GetView("TP_РаботаВЗаказеL", typeof(IIS.AutoService.РаботаВЗаказе));
                }
            }
        }
    }
    
    /// <summary>
    /// Detail array of РаботаВЗаказе.
    /// </summary>
    // *** Start programmer edit section *** (DetailArrayDetailArrayOfРаботаВЗаказе CustomAttributes)

    // *** End programmer edit section *** (DetailArrayDetailArrayOfРаботаВЗаказе CustomAttributes)
    public class DetailArrayOfРаботаВЗаказе : ICSSoft.STORMNET.DetailArray
    {
        
        // *** Start programmer edit section *** (IIS.AutoService.DetailArrayOfРаботаВЗаказе members)

        // *** End programmer edit section *** (IIS.AutoService.DetailArrayOfРаботаВЗаказе members)

        
        /// <summary>
        /// Construct detail array.
        /// </summary>
        /// <summary>
        /// Returns object with type РаботаВЗаказе by index.
        /// </summary>
        /// <summary>
        /// Adds object with type РаботаВЗаказе.
        /// </summary>
        public DetailArrayOfРаботаВЗаказе(IIS.AutoService.Заказ fЗаказ) : 
                base(typeof(РаботаВЗаказе), ((ICSSoft.STORMNET.DataObject)(fЗаказ)))
        {
        }
        
        public IIS.AutoService.РаботаВЗаказе this[int index]
        {
            get
            {
                return ((IIS.AutoService.РаботаВЗаказе)(this.ItemByIndex(index)));
            }
        }
        
        public virtual void Add(IIS.AutoService.РаботаВЗаказе dataobject)
        {
            this.AddObject(((ICSSoft.STORMNET.DataObject)(dataobject)));
        }
    }
}
