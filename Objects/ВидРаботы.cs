﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IIS.AutoService
{
    using System;
    using System.Xml;
    using ICSSoft.STORMNET;
    
    
    // *** Start programmer edit section *** (Using statements)

    // *** End programmer edit section *** (Using statements)


    /// <summary>
    /// Вид работы.
    /// </summary>
    // *** Start programmer edit section *** (ВидРаботы CustomAttributes)

    // *** End programmer edit section *** (ВидРаботы CustomAttributes)
    [AutoAltered()]
    [Caption("Вид работы")]
    [AccessType(ICSSoft.STORMNET.AccessType.none)]
    [View("TP_ВидРаботыE", new string[] {
            "Название as \'Название\'",
            "Стоимость as \'Стоимость\'",
            "Актуально as \'Актуально\'"})]
    [View("TP_ВидРаботыL", new string[] {
            "Название as \'Название\'",
            "Стоимость as \'Стоимость\'",
            "Актуально as \'Актуально\'"})]
    public class ВидРаботы : ICSSoft.STORMNET.DataObject
    {
        
        private string fНазвание;
        
        private int fСтоимость;
        
        private bool fАктуально;
        
        // *** Start programmer edit section *** (ВидРаботы CustomMembers)

        // *** End programmer edit section *** (ВидРаботы CustomMembers)

        
        /// <summary>
        /// Название.
        /// </summary>
        // *** Start programmer edit section *** (ВидРаботы.Название CustomAttributes)

        // *** End programmer edit section *** (ВидРаботы.Название CustomAttributes)
        [StrLen(255)]
        [NotNull()]
        public virtual string Название
        {
            get
            {
                // *** Start programmer edit section *** (ВидРаботы.Название Get start)

                // *** End programmer edit section *** (ВидРаботы.Название Get start)
                string result = this.fНазвание;
                // *** Start programmer edit section *** (ВидРаботы.Название Get end)

                // *** End programmer edit section *** (ВидРаботы.Название Get end)
                return result;
            }
            set
            {
                // *** Start programmer edit section *** (ВидРаботы.Название Set start)

                // *** End programmer edit section *** (ВидРаботы.Название Set start)
                this.fНазвание = value;
                // *** Start programmer edit section *** (ВидРаботы.Название Set end)

                // *** End programmer edit section *** (ВидРаботы.Название Set end)
            }
        }
        
        /// <summary>
        /// Стоимость.
        /// </summary>
        // *** Start programmer edit section *** (ВидРаботы.Стоимость CustomAttributes)

        // *** End programmer edit section *** (ВидРаботы.Стоимость CustomAttributes)
        [NotNull()]
        public virtual int Стоимость
        {
            get
            {
                // *** Start programmer edit section *** (ВидРаботы.Стоимость Get start)

                // *** End programmer edit section *** (ВидРаботы.Стоимость Get start)
                int result = this.fСтоимость;
                // *** Start programmer edit section *** (ВидРаботы.Стоимость Get end)

                // *** End programmer edit section *** (ВидРаботы.Стоимость Get end)
                return result;
            }
            set
            {
                // *** Start programmer edit section *** (ВидРаботы.Стоимость Set start)

                // *** End programmer edit section *** (ВидРаботы.Стоимость Set start)
                this.fСтоимость = value;
                // *** Start programmer edit section *** (ВидРаботы.Стоимость Set end)

                // *** End programmer edit section *** (ВидРаботы.Стоимость Set end)
            }
        }
        
        /// <summary>
        /// Актуально.
        /// </summary>
        // *** Start programmer edit section *** (ВидРаботы.Актуально CustomAttributes)

        // *** End programmer edit section *** (ВидРаботы.Актуально CustomAttributes)
        public virtual bool Актуально
        {
            get
            {
                // *** Start programmer edit section *** (ВидРаботы.Актуально Get start)

                // *** End programmer edit section *** (ВидРаботы.Актуально Get start)
                bool result = this.fАктуально;
                // *** Start programmer edit section *** (ВидРаботы.Актуально Get end)

                // *** End programmer edit section *** (ВидРаботы.Актуально Get end)
                return result;
            }
            set
            {
                // *** Start programmer edit section *** (ВидРаботы.Актуально Set start)

                // *** End programmer edit section *** (ВидРаботы.Актуально Set start)
                this.fАктуально = value;
                // *** Start programmer edit section *** (ВидРаботы.Актуально Set end)

                // *** End programmer edit section *** (ВидРаботы.Актуально Set end)
            }
        }
        
        /// <summary>
        /// Class views container.
        /// </summary>
        public class Views
        {
            
            /// <summary>
            /// "TP_ВидРаботыE" view.
            /// </summary>
            public static ICSSoft.STORMNET.View TP_ВидРаботыE
            {
                get
                {
                    return ICSSoft.STORMNET.Information.GetView("TP_ВидРаботыE", typeof(IIS.AutoService.ВидРаботы));
                }
            }
            
            /// <summary>
            /// "TP_ВидРаботыL" view.
            /// </summary>
            public static ICSSoft.STORMNET.View TP_ВидРаботыL
            {
                get
                {
                    return ICSSoft.STORMNET.Information.GetView("TP_ВидРаботыL", typeof(IIS.AutoService.ВидРаботы));
                }
            }
        }
    }
}
