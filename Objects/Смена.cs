﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IIS.AutoService
{
    using System;
    using System.Xml;
    using ICSSoft.STORMNET;
    
    
    // *** Start programmer edit section *** (Using statements)

    // *** End programmer edit section *** (Using statements)


    /// <summary>
    /// Смена.
    /// </summary>
    // *** Start programmer edit section *** (Смена CustomAttributes)

    // *** End programmer edit section *** (Смена CustomAttributes)
    [AutoAltered()]
    [AccessType(ICSSoft.STORMNET.AccessType.none)]
    [View("TP_СменаE", new string[] {
            "ДатаИВремяНачала as \'Дата и время начала\'",
            "ДатаИВремяКонца as \'Дата и время конца\'",
            "Выручка as \'Выручка\'",
            "Менеджер as \'Менеджер\'",
            "Менеджер.Номер as \'Номер менеджера\'",
            "Менеджер.Фамилия as \'Фамилия\'"})]
    [MasterViewDefineAttribute("TP_СменаE", "Менеджер", ICSSoft.STORMNET.LookupTypeEnum.Standard, "", "Фамилия")]
    [View("TP_СменаL", new string[] {
            "Менеджер.Номер as \'Номер менеджера\'",
            "Менеджер.Фамилия as \'Фамилия\'",
            "ДатаИВремяНачала as \'Дата и время начала\'",
            "ДатаИВремяКонца as \'Дата и время конца\'",
            "Выручка as \'Выручка\'"})]
    public class Смена : ICSSoft.STORMNET.DataObject
    {
        
        private System.DateTime? fДатаИВремяНачала;
        
        private System.DateTime? fДатаИВремяКонца;
        
        private IIS.AutoService.Сотрудник fМенеджер;
        
        // *** Start programmer edit section *** (Смена CustomMembers)

        // *** End programmer edit section *** (Смена CustomMembers)

        
        /// <summary>
        /// ДатаИВремяНачала.
        /// </summary>
        // *** Start programmer edit section *** (Смена.ДатаИВремяНачала CustomAttributes)

        // *** End programmer edit section *** (Смена.ДатаИВремяНачала CustomAttributes)
        [NotNull()]
        public virtual System.DateTime? ДатаИВремяНачала
        {
            get
            {
                // *** Start programmer edit section *** (Смена.ДатаИВремяНачала Get start)

                // *** End programmer edit section *** (Смена.ДатаИВремяНачала Get start)
                System.DateTime? result = this.fДатаИВремяНачала;
                // *** Start programmer edit section *** (Смена.ДатаИВремяНачала Get end)

                // *** End programmer edit section *** (Смена.ДатаИВремяНачала Get end)
                return result;
            }
            set
            {
                // *** Start programmer edit section *** (Смена.ДатаИВремяНачала Set start)

                // *** End programmer edit section *** (Смена.ДатаИВремяНачала Set start)
                this.fДатаИВремяНачала = value;
                // *** Start programmer edit section *** (Смена.ДатаИВремяНачала Set end)

                // *** End programmer edit section *** (Смена.ДатаИВремяНачала Set end)
            }
        }
        
        /// <summary>
        /// ДатаИВремяКонца.
        /// </summary>
        // *** Start programmer edit section *** (Смена.ДатаИВремяКонца CustomAttributes)

        // *** End programmer edit section *** (Смена.ДатаИВремяКонца CustomAttributes)
        public virtual System.DateTime? ДатаИВремяКонца
        {
            get
            {
                // *** Start programmer edit section *** (Смена.ДатаИВремяКонца Get start)

                // *** End programmer edit section *** (Смена.ДатаИВремяКонца Get start)
                System.DateTime? result = this.fДатаИВремяКонца;
                // *** Start programmer edit section *** (Смена.ДатаИВремяКонца Get end)

                // *** End programmer edit section *** (Смена.ДатаИВремяКонца Get end)
                return result;
            }
            set
            {
                // *** Start programmer edit section *** (Смена.ДатаИВремяКонца Set start)

                // *** End programmer edit section *** (Смена.ДатаИВремяКонца Set start)
                this.fДатаИВремяКонца = value;
                // *** Start programmer edit section *** (Смена.ДатаИВремяКонца Set end)

                // *** End programmer edit section *** (Смена.ДатаИВремяКонца Set end)
            }
        }
        
        /// <summary>
        /// Выручка.
        /// </summary>
        // *** Start programmer edit section *** (Смена.Выручка CustomAttributes)

        // *** End programmer edit section *** (Смена.Выручка CustomAttributes)
        [ICSSoft.STORMNET.NotStored()]
        public virtual int Выручка
        {
            get
            {
                // *** Start programmer edit section *** (Смена.Выручка Get)
                return 0;
                // *** End programmer edit section *** (Смена.Выручка Get)
            }
            set
            {
                // *** Start programmer edit section *** (Смена.Выручка Set)

                // *** End programmer edit section *** (Смена.Выручка Set)
            }
        }
        
        /// <summary>
        /// Смена.
        /// </summary>
        // *** Start programmer edit section *** (Смена.Менеджер CustomAttributes)

        // *** End programmer edit section *** (Смена.Менеджер CustomAttributes)
        [PropertyStorage(new string[] {
                "Менеджер"})]
        [NotNull()]
        public virtual IIS.AutoService.Сотрудник Менеджер
        {
            get
            {
                // *** Start programmer edit section *** (Смена.Менеджер Get start)

                // *** End programmer edit section *** (Смена.Менеджер Get start)
                IIS.AutoService.Сотрудник result = this.fМенеджер;
                // *** Start programmer edit section *** (Смена.Менеджер Get end)

                // *** End programmer edit section *** (Смена.Менеджер Get end)
                return result;
            }
            set
            {
                // *** Start programmer edit section *** (Смена.Менеджер Set start)

                // *** End programmer edit section *** (Смена.Менеджер Set start)
                this.fМенеджер = value;
                // *** Start programmer edit section *** (Смена.Менеджер Set end)

                // *** End programmer edit section *** (Смена.Менеджер Set end)
            }
        }
        
        /// <summary>
        /// Class views container.
        /// </summary>
        public class Views
        {
            
            /// <summary>
            /// "TP_СменаE" view.
            /// </summary>
            public static ICSSoft.STORMNET.View TP_СменаE
            {
                get
                {
                    return ICSSoft.STORMNET.Information.GetView("TP_СменаE", typeof(IIS.AutoService.Смена));
                }
            }
            
            /// <summary>
            /// "TP_СменаL" view.
            /// </summary>
            public static ICSSoft.STORMNET.View TP_СменаL
            {
                get
                {
                    return ICSSoft.STORMNET.Information.GetView("TP_СменаL", typeof(IIS.AutoService.Смена));
                }
            }
        }
    }
}
